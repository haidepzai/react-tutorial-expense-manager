import React, { Fragment, useState } from 'react';
import AddUser from './components/Users/AddUser';
import UsersList from './components/Users/UsersList';


function App() {
  const [usersList, setUsersList] = useState([]);

  const addUserHandler = (username, age) => {
    setUsersList((prevUsersList) => {
      return [...prevUsersList, {name: username, age: age, id: Math.random().toString()}];
    });
  }

  return (
    <Fragment>    
      	<AddUser onAddUser={addUserHandler} //In Angular: Event-Binding (onAddUser)="addUserHandler($event)"
        />
        <UsersList users={usersList}/>
    </Fragment>
  );
}

export default App;
