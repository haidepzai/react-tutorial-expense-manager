import classes from './Card.module.css';

const Card = props => {
    return (//props.children = Inhalt innerhalb <Card></Card>
        <div className={`${classes.card} ${props.className}`}>{props.children}</div>
    )
};

export default Card;