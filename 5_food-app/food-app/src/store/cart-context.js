import React from 'react';

//Property of items:
/*
id: props.id,
name: props.name,
amount: amount,
price: props.price
*/
const CartContext = React.createContext({
    items: [],
    totalAmount: 0,
    addItem: (item) => {},
    removeItem: (id) => {},
    clearCart: () => {}
});

export default CartContext;