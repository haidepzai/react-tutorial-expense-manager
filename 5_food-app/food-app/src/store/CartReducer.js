// Logik wo der State verändert wird
const cartReducer = (state, action) => {
    if (action.type === 'ADD') {
        const updatedTotalAmount = state.totalAmount + action.item.price * action.item.amount;

        //Check if Item already exist in Cart
        const existingCartItemIndex = state.items.findIndex(item => item.id === action.item.id);
        const existingCartItem = state.items[existingCartItemIndex];

        let updatedItems;

        //Falls es ein Existing Item gibt:
        //Den Betrag des Existing Item ändern
        //Dieses geupdate Item dann in der Liste Items ersetzen
        if (existingCartItem) {
            const updatedItem = {
                ...existingCartItem,
                amount: existingCartItem.amount + action.item.amount // Counter erhöhen
            }
            updatedItems = [...state.items];
            updatedItems[existingCartItemIndex] = updatedItem
        } //Wenn zum 1. mal
        else {
            updatedItems = state.items.concat(action.item);
        }

        return {
            items: updatedItems,
            totalAmount: updatedTotalAmount
        };
    }

    if (action.type === 'REMOVE') {
        //Check if Item already exist in Cart
        const existingCartItemIndex = state.items.findIndex(item => item.id === action.id);
        const existingCartItem = state.items[existingCartItemIndex];
        const updatedTotalAmount = state.totalAmount - existingCartItem.price;

        let updatedItems;
        // Remove Item completely from Array
        if (existingCartItem.amount === 1) {
            updatedItems = state.items.filter(item => item.id !== action.id);
        }
        // Keep Item in array but decrease amount
        else {
            const updatedItem = { ...existingCartItem, amount: existingCartItem.amount - 1 };
            updatedItems = [...state.items];
            updatedItems[existingCartItemIndex] = updatedItem
        }

        return {
            items: updatedItems,
            totalAmount: updatedTotalAmount
        };

    }

    if (action.type === 'CLEAR') {
        return state;
    }

    return state;
};

export default cartReducer;