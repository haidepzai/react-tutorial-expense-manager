import React, { Fragment } from 'react'

import mealsImage from '../../assets/sushi.png';
import classes from './Header.module.css';
import HeaderCartButton from './HeaderCartButton';

const Header = (props) => {
  return (
    <Fragment>
        <header className={classes.header}>
            <h1>Hai Gourmet</h1>
            <HeaderCartButton onClick={props.onShowCart}/>
        </header>
        <button>Cart</button>
        <div className={classes['main-image']}>
            <img src={mealsImage} alt="A table full of delicious food!" />
        </div>
    </Fragment>
  )
}

export default Header