import React, { useState, useReducer, useEffect, useContext, useRef } from 'react';

import Card from '../UI/Card/Card';
import classes from './Login.module.css';
import Button from '../UI/Button/Button';
import AuthContext from '../../context/auth-context';
import Input from '../UI/Input/Input';

// Reducer um mehrere States zu handeln
// Komplexere Update State Logik
const emailReducer = (state, action) => {
  if (action.type === "USER_INPUT") {
    console.log("Input: " + action.val); // Bei jedem Keystroke
    // Man updated enteredEmail und emailIsValid in einem Run
    return { value: action.val, isValid: action.val.includes('@') };
  }
  //state.value ist latest value
  if (action.type === "INPUT_BLUR") {
    console.log("Blur: " + state.value); // Beim Verlassen des Input Felds
    return { value: state.value, isValid: state.value.includes('@') };
  }
  return { value: '', isValid: false };
};

const passwordReducer = (state, action) => {
  if (action.type === "USER_INPUT") {
    // Man updated enteredPassword und passwordIsValid in einem Run
    return { value: action.val, isValid: action.val.trim().length > 6 };
  }
  //state.value ist latest value
  if (action.type === "INPUT_BLUR") {
    return { value: state.value, isValid: state.value.trim().length > 6 };
  }
  return { value: '', isValid: false };
}

const Login = (props) => {
  // const [enteredEmail, setEnteredEmail] = useState('');
  // const [emailIsValid, setEmailIsValid] = useState();
  // const [enteredPassword, setEnteredPassword] = useState('');
  // const [passwordIsValid, setPasswordIsValid] = useState();
  const [formIsValid, setFormIsValid] = useState(false);

  //useReducer(<reducer>, <initialState>)
  const [emailState, dispatchEmail] = useReducer(emailReducer, {
    value: '',
    isValid: null
  });
  const [passwordState, dispatchPassword] = useReducer(passwordReducer, {
    value: '',
    isValid: null
  });

  const authCtx = useContext(AuthContext);

  const emailInputRef = useRef();
  const passwordInputRef = useRef();

  // Object Destructuring : pull out certain properties from object
  const { isValid: emailIsValid } = emailState; //Alias Assignment emailIsValid
  const { isValid: passwordIsValid } = passwordState //Allias Assignment not Value Assignment!!

  //Re-run logic when certain data change
  useEffect(() => {
    //Beim ersten mal läuft das immer!
    const identifier = setTimeout(() => { //To ensure that this will not invoke after every key stroke (debounce)
      console.log("Checking form validity");
      setFormIsValid(
        emailIsValid && passwordIsValid // Wenn beide True sind
      );
    }, 500);

    // clean up function (will run before the actual useEffect - except for the first time)
    // Läuft nicht beim 1. mal!!
    // Läuft noch vor dem 1. Side Effect Execution!
    return () => {
      console.log("CLEANUP");
      clearTimeout(identifier); //Clear the last timer before a new one is set
    }; // Run before every new side effect execution and before component removed
  }, [emailIsValid, passwordIsValid]); //Effect wird getriggert. sobald ein Wert sich ändert

  //Flow:
  //1. Mal läuft useEffect ganz normal
  //2. Keystroke wird Cleanup aktiviert (Timeout wird entfernt)
  //3. Dann wird der Effekt nach 5ms ausgeführt


  const emailChangeHandler = (event) => {
    dispatchEmail({ type: 'USER_INPUT', val: event.target.value }); //trigger emailReducer function

    setFormIsValid(
      emailState.isValid && passwordState.isValid
    );
  };

  const passwordChangeHandler = (event) => {
    dispatchPassword({ type: 'USER_INPUT', val: event.target.value });

    setFormIsValid(
      passwordState.isValid && emailState.isValid
    );
  };
  //Wird aufgerufen, wenn man mit der Maus aus dem Input Feld rausgeht
  const validateEmailHandler = () => {
    dispatchEmail({ type: 'INPUT_BLUR' }); // no further payload
  };

  //Wird aufgerufen, wenn man mit der Maus aus dem Input Feld rausgeht
  const validatePasswordHandler = () => {
    dispatchPassword({ type: 'INPUT_BLUR' });
  };

  const submitHandler = (event) => {
    event.preventDefault();
    if (formIsValid) {
      authCtx.onLogin(emailState.value, passwordState.value);
    } else if (!emailIsValid) {
      emailInputRef.current.focus();
    } else {
      passwordInputRef.current.focus();
    }
  };

  return (
    <Card className={classes.login}>
      <form onSubmit={submitHandler}>
        <Input
          ref={emailInputRef}
          id="email"
          label="E-Mail"
          type="email"
          isValid={emailIsValid}
          value={emailState.value}
          onChange={emailChangeHandler}
          onBlur={validateEmailHandler}
        />
        <Input
          ref={passwordInputRef}
          id="password"
          label="Password"
          type="password"
          isValid={passwordIsValid}
          value={passwordState.value}
          onChange={passwordChangeHandler}
          onBlur={validatePasswordHandler}
        />
        <div className={classes.actions}>
          <Button type="submit" className={classes.btn}>
            Login
          </Button>
        </div>
      </form>
    </Card>
  );
};

export default Login;
