import React, { useCallback, useEffect, useState } from 'react';

import MoviesList from './components/MoviesList';
import AddMovie from './components/AddMovie';
import './App.css';

function App() {
  const [movies, setMovies] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  /*
  useCallback speichert einen alten Snapshot, 
  deswegen ist in der Funktion noch der alte Wert gespeichert (Javascript Closure) 
  - Nur wenn Dependency sich ändert, wird Funktion nochmal aktiviert, sonst wird das jedes mal aufgerufen,
  wenn Component neu rendert.
  */
  const fetchMoviesHandler = useCallback(() => { //returns a Promise
    // Hier mit Promise
    setIsLoading(true);
    setError(null);
    // Then (fetch ist ein Promise)
    fetch('https://react-http-1a596-default-rtdb.europe-west1.firebasedatabase.app/movies.json').then(response => {
      if (!response.ok) {
        throw new Error('Something went wrong');
      };
      return response.json();
    }).then(data => {
      const loadedMovies = [];

      for (const key in data) {
        loadedMovies.push({
          id: key,
          title: data[key].title,
          openingText: data[key].openingText,
          releaseDate: data[key].releaseDate
        });
      };

      setMovies(loadedMovies);
      setIsLoading(false);
    }).catch(error => {
      setError(error.message);
      setIsLoading(false);
    });
  }, []);

  useEffect(() => {
    fetchMoviesHandler();
  }, [fetchMoviesHandler]);

  async function addMovieHandler(movie) {
    // POST Request
    //Hier mit async-await
    try {
      const response = await fetch('https://react-http-1a596-default-rtdb.europe-west1.firebasedatabase.app/movies.json', {
        method: 'POST',
        body: JSON.stringify(movie),
        headers: {
          'Content-Type': 'application/json'
        }
      });
      if (!response.ok) {
        throw new Error('Something went wrong');
      };
      const data = await response.json();
      console.log(data);
    } catch (e) {
      console.log(e);
    };
  };

  let content = <p>Found no movies.</p>;

  if (movies.length > 0) {
    content = <MoviesList movies={movies} />
  }
  if (error) {
    content = <p>{error}</p>
  }
  if (isLoading) {
    content = <p>Loading...</p>
  }


  return (
    <React.Fragment>
      <section>
        <AddMovie onAddMovie={addMovieHandler} />
      </section>
      <section>
        <button onClick={fetchMoviesHandler}>Fetch Movies</button>
      </section>
      <section>
        {content}
      </section>
    </React.Fragment>
  );
}

export default App;
