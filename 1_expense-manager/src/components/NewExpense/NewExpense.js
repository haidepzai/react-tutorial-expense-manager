import { useState } from 'react';
import ExpenseForm from './ExpenseForm';
import './NewExpense.css';

const NewExpense = (props) => {
    const [isEditing, setIsEditing] = useState(false);

    const saveExpenseDataHandler = (enteredExpenseData) => {
        const expenseData = {
            ...enteredExpenseData, //title, amount, date object
            id: Math.random().toString()
        };
        props.onAddExpense(expenseData); //Funktion addExpenseHandler in App.js wird aufgerufen (expenseData kommt von Child)
    };

    const startEditingHandler = () => {
        setIsEditing(true);
    };

    const stopEditingHandler = () => {
        setIsEditing(false);
    }


    return (
        <div className='new-expense'>
            {!isEditing && <button onClick={startEditingHandler}>Add New Expense</button>}
            {isEditing && <ExpenseForm onSaveExpenseData={saveExpenseDataHandler} //Pointer auf saveExpenseDataHandler wird dem Kind übergeben
            //Angular: Event-Binding : (onSaveExpenseData)="saveExpenseDataHandler($event)"
            //Event Binding: Retrieve the Data which got emitted from the child component (Output)
            onCancel={stopEditingHandler} //onCancel übergeben an Kind Component mit einem Pointer auf stopEditingHandler
            />}
        </div>
    );
};

export default NewExpense;