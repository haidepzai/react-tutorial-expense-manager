import { useReducer, useState } from "react";

const initialInputState = {
    value: '',
    isTouched: false
}

const inputStateReducer = (state, action) => {
    if (action.type === 'INPUT') {
        return { value: action.value, isTouched: state.isTouched }; //state.isTouched = Previous State of isTouched
    }
    if (action.type === 'BLUR') {
        return { isTouched: true, value: state.value} //state.value = existing value
    }
    if (action.type === 'RESET') {
        return { isTouched: false, value: ''}
    }
    return initialInputState;
}

const useInput = (validateValue) => {
    //Mit State:
    const [enteredValue, setEnteredValue] = useState('');
    const [isTouched, setIsTouched] = useState(false);

    //Mit Reducer
    const [inputState, dispatch] = useReducer(inputStateReducer, initialInputState);

    //const valueIsValid = validateValue(enteredValue);
    //const hasError = !valueIsValid && isTouched;
    const valueIsValid = validateValue(inputState.value);
    const hasError = !valueIsValid && inputState.isTouched;

    const valueChangeHandler = event => {
        dispatch({type: 'INPUT', value: event.target.value});
        //setEnteredValue(event.target.value);
    };

    const inputBlurHandler = event => {
        dispatch({type: 'BLUR'});
        //setIsTouched(true);
    };

    const reset = () => {
        dispatch({type: 'RESET'});
        //setEnteredValue('');
        //setIsTouched(false);
    };

    return {
        //value: enteredValue,
        value: inputState.value,
        isValid: valueIsValid,
        hasError,
        valueChangeHandler,
        inputBlurHandler,
        reset
    };

};

export default useInput;