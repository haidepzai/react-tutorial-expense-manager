import useInput from "../hooks/use-input";

const SimpleInput = (props) => {
  //Name
  const {
    value: enteredName, 
    isValid: enteredNameIsValid,
    hasError: nameInputHasError, 
    valueChangeHandler: nameChangedHandler, 
    inputBlurHandler: nameBlurHandler,
    reset: resetNameInput
  } = useInput(value => value.trim() !== ''); //Function definition only
  //Email
  const {
    value: enteredEmail, 
    isValid: enteredEmailIsValid,
    hasError: emailInputHasError, 
    valueChangeHandler: emailChangedHandler, 
    inputBlurHandler: emailBlurHandler,
    reset: resetEmailInput
  } = useInput(value => value.includes('@')); //Function definition only

  let formIsValid = false; 
  //Braucht man keinen Hook, weil der Wert so oder so neu evaluiert wird mit jedem Keystroke bei den Inputs

  //alle inputs checken
  if (enteredNameIsValid && enteredEmailIsValid) { //Falls alle Inputs valid sind
    formIsValid = true;
  }

  const formSubmissionHandler = event => {
    event.preventDefault();

    if (!enteredNameIsValid) {
      return;
    };

    console.log(enteredName);

    resetNameInput('');
    resetEmailInput('');
  };

  const nameInputClasses = nameInputHasError ? 'form-control invalid' : 'form-control';

  const emailInputClasses = nameInputHasError ? 'form-control invalid' : 'form-control';

  return (
    <form onSubmit={formSubmissionHandler}>
      <div className={nameInputClasses}>
        <label htmlFor='name'>Your Name</label>
        <input
          type='text'
          id='name'
          onChange={nameChangedHandler}
          onBlur={nameBlurHandler} // Wird getriggert, wenn man aus dem Input rausgeht
          value={enteredName} //two-way-binding
        />
        {nameInputHasError && <p className="error-text">Name must not be empty.</p>}
      </div>
      <div className={emailInputClasses}>
        <label htmlFor='email'>Your E-Mail</label>
        <input
          type='email'
          id='email'
          onChange={emailChangedHandler}
          onBlur={emailBlurHandler} // Wird getriggert, wenn man aus dem Input rausgeht
          value={enteredEmail} //two-way-binding
        />
        {emailInputHasError && <p className="error-text">Please enter a valid E-Mail</p>}
      </div>
      <div className="form-actions">
        <button disabled={!formIsValid}>Submit</button>
      </div>
    </form>
  );
};

export default SimpleInput;
